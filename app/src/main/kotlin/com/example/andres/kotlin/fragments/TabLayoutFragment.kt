package com.example.andres.kotlin.fragments

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.andres.kotlin.R
import com.example.andres.kotlin.fragments.base.BaseTabLayoutFragment

import java.util.ArrayList
import java.util.Arrays

/**
 * Created by Andres on 25/07/2016.
 */
class TabLayoutFragment : android.support.v4.app.Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater!!.inflate(R.layout.fragment_tab_layout, container, false)

        val viewPager = rootView.findViewById(R.id.viewpager) as ViewPager

        val adapter = ViewPagerAdapter(childFragmentManager)

        val ROOT_IMAGE = "rootImage"
        val CAPTION = "text"

        val titlesHistory = resources.getStringArray(R.array.titles_os)
        val life = resources.getStringArray(R.array.os)

        for (i in 1..titlesHistory.size) {

            val bundle = Bundle()
            bundle.putInt(ROOT_IMAGE, context.resources.getIdentifier("img" + i, "drawable", context.packageName))
            bundle.putString(CAPTION, Arrays.asList(*life)[i - 1])
            val baseHistoryFragment = BaseTabLayoutFragment()
            baseHistoryFragment.arguments = bundle
            adapter.addFragment(baseHistoryFragment, Arrays.asList(*titlesHistory)[i - 1])

        }

        viewPager.adapter = adapter
        val tabLayout = rootView.findViewById(R.id.tabs) as TabLayout
        tabLayout.setupWithViewPager(viewPager)

        return rootView
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            val bundle = Bundle()
            bundle.putInt("rootImage", fragment.arguments.getInt("rootImage"))
            bundle.putString("text", fragment.arguments.getString("text", ""))
            fragment.arguments = bundle
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }
}
