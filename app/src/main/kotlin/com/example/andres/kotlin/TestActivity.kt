package com.example.andres.kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        val closeSession = findViewById(R.id.button) as Button

        closeSession.setOnClickListener {
            startActivity(Intent(this, MainActivityExample::class.java))
        }
   }
}
