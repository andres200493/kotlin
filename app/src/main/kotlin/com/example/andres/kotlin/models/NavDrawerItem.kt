package com.example.andres.kotlin.models

import android.widget.ImageView

/**
 * Created by Andres on 25/07/2016.
 */
class NavDrawerItem {
    var isShowNotify: Boolean = false
    var title: String? = null
    var icon: ImageView? = null


    constructor() {

    }

    constructor(showNotify: Boolean, title: String, icon: ImageView) {
        this.isShowNotify = showNotify
        this.title = title
        this.icon = icon
    }

}