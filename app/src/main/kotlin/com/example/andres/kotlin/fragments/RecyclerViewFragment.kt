package com.example.andres.kotlin.fragments

import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.andres.kotlin.R
import java.util.*

/**
 * Created by Andres on 25/07/2016.
 */
class RecyclerViewFragment : android.support.v4.app.Fragment() {

    private var rootView: View? = null
    private var recyclerView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        rootView = inflater!!.inflate(R.layout.fragment_recyclerview, container, false)

        recyclerView = rootView!!.findViewById(R.id.recycler_view) as RecyclerView

        recyclerView!!.setHasFixedSize(true)

        val osList = ArrayList<osClass>()
        osList.add(osClass("Android", "Mobile", R.drawable.img1))
        osList.add(osClass("IOS", "Mobile", R.drawable.img2))
        osList.add(osClass("Windows", "Desk", R.drawable.img3))
        osList.add(osClass("Linux", "Desk", R.drawable.img4))

        val adapter = RVAdapter(osList)
        recyclerView!!.adapter = adapter

        val linearLayoutManager = LinearLayoutManager(context)
        recyclerView!!.layoutManager = linearLayoutManager

        return rootView
    }

    /*****************************************************
     * CLASE FOR DATES
     */
    internal inner class osClass(var name: String, var type: String, var imageView: Int)

    /*****************************************************
     * ADAPTER
     */

    inner class RVAdapter internal constructor(internal var adviceList:

                                               List<osClass>) : RecyclerView.Adapter<RVAdapter.PersonViewHolder>() {

        override fun getItemCount(): Int {
            return adviceList.size
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): PersonViewHolder {
            val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_cardview, viewGroup, false)
            return PersonViewHolder(v)
        }

        override fun onBindViewHolder(personViewHolder: PersonViewHolder, i: Int) {
            personViewHolder.name.text = adviceList[i].name
            personViewHolder.type.text = adviceList[i].type
            personViewHolder.imageView.setImageResource(context.resources.getIdentifier(
                    "img" + (i + 1), "drawable", context.packageName))
        }

        override fun onAttachedToRecyclerView(recyclerView: RecyclerView?) {
            super.onAttachedToRecyclerView(recyclerView)
        }

        inner class PersonViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var cv: CardView
            internal var name: TextView
            internal var type: TextView
            internal var imageView: ImageView

            init {
                cv = itemView.findViewById(R.id.cv) as CardView
                imageView = itemView.findViewById(R.id.os_photo) as ImageView
                name = itemView.findViewById(R.id.os_name) as TextView
                type = itemView.findViewById(R.id.os_type) as TextView
            }
        }
    }
}
