package com.example.andres.kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivityExample : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_example)

        val button = findViewById(R.id.button) as Button
        val edtEmail = findViewById(R.id.input_email) as EditText
        val edtPass = findViewById(R.id.input_password) as EditText

        button.setOnClickListener {
            if(edtEmail.text.isEmpty()){
                Toast.makeText(this@MainActivityExample, "Check Email", Toast.LENGTH_SHORT).show()
            } else if(edtPass.text.isEmpty()){
                Toast.makeText(this@MainActivityExample, "Check Password", Toast.LENGTH_SHORT).show()
            } else {
                startActivity(Intent(this, TestActivity::class.java))
            }
        }
    }
}
