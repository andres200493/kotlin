package com.example.andres.kotlin.fragments.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.example.andres.kotlin.R

/**
 * Created by Andres on 01/08/2016.
 */
class BaseTabLayoutFragment : Fragment() {

    private var rootView: View? = null
    private var imvImage: ImageView? = null
    private var txvCaption: TextView? = null

    private var resource: Int = 0
    private var text: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        resource = arguments.getInt("rootImage")
        text = arguments.getString("text")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater!!.inflate(R.layout.fragment_base_tab_layout, container, false)

        init()

        imvImage!!.setImageResource(resource)
        txvCaption!!.text = text

        return rootView
    }

    private fun init() {
        imvImage = rootView!!.findViewById(R.id.image_history) as ImageView
        txvCaption = rootView!!.findViewById(R.id.caption_history) as TextView
    }
}
