package com.example.andres.kotlin.activities

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.RelativeLayout
import com.example.andres.kotlin.R
import com.example.andres.kotlin.fragments.*

/**
 * Created by Andres on 25/07/2016.
 */
class MainActivity : AppCompatActivity(), DrawerFragment.FragmentDrawerListener {

    var mToolbar: Toolbar? = null
    var drawerFragment: DrawerFragment? = null
    var fragmentManager: FragmentManager? = null
    var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mToolbar = findViewById(R.id.toolbar) as Toolbar
        val closeSession = findViewById(R.id.close_session) as RelativeLayout

        closeSession.setOnClickListener {
            val alertDialog = AlertDialog.Builder(this@MainActivity)
            alertDialog.setTitle(R.string.exit)
            alertDialog.setMessage(R.string.confirm_exit)
            alertDialog.setPositiveButton(R.string.yes) { dialog, which -> finish() }
            alertDialog.setNegativeButton(R.string.no) { dialog, which -> }
            alertDialog.show()
        }

        fragmentManager = supportFragmentManager

        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        drawerFragment = supportFragmentManager.findFragmentById(R.id.fragment_navigation_drawer) as DrawerFragment
        drawerFragment!!.setUp(R.id.fragment_navigation_drawer, findViewById(R.id.drawer_layout) as DrawerLayout, mToolbar!!)
        drawerFragment!!.setDrawerListener(this)

        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragment = HomeFragment()
        fragmentTransaction.replace(R.id.container_body, fragment, "home").commit()
    }

    override fun onDrawerItemSelected(view: View, position: Int) {
        var title = getString(R.string.app_name)
        var tag = "null"
        when (position) {
            0 -> {
                fragment = TabLayoutFragment()
                tag = "tab"
                title = getString(R.string.nav_item_tab)
            }
            1 -> {
                fragment = ListViewFragment()
                tag = "list"
                title = getString(R.string.nav_item_list)
            }
            2 -> {
                fragment = RecyclerViewFragment()
                tag = "recycler"
                title = getString(R.string.nav_item_recycler)
            }
        }

        if (fragment != null) {
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            val currentTag = supportFragmentManager.findFragmentById(R.id.container_body).tag
            if (currentTag != "home") {
                fragmentTransaction.remove(supportFragmentManager.findFragmentByTag(currentTag))
            }
            fragmentTransaction.add(R.id.container_body, fragment, tag).commit()

            supportActionBar!!.title = title
        }
    }

    override fun onBackPressed() {
        val currentTag = supportFragmentManager.findFragmentById(R.id.container_body).tag

        if (drawerFragment!!.isVisible) {
            val mDrawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout
            mDrawerLayout.closeDrawers()
        } else {
            when (currentTag) {
                "home" -> {
                    val alertDialog = AlertDialog.Builder(this)
                    alertDialog.setTitle(R.string.exit)
                    alertDialog.setMessage(R.string.confirm_exit)
                    alertDialog.setPositiveButton(R.string.yes) { dialog, which -> finish() }
                    alertDialog.setNegativeButton(R.string.no) { dialog, which -> }
                    alertDialog.show()
                }
                else -> {
                    supportFragmentManager.beginTransaction().remove(supportFragmentManager.findFragmentByTag(currentTag)).commit()
                    supportActionBar!!.setTitle(R.string.app_name)
                }
            }
        }
    }
}