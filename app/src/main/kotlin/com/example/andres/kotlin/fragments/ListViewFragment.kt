package com.example.andres.kotlin.fragments

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast

import kotlinx.android.synthetic.main.fragment_listview.*

import com.example.andres.kotlin.R
import com.example.andres.kotlin.commons.*

/**
 * Created by Andres on 25/07/2016.
 */
class ListViewFragment : android.support.v4.app.Fragment() {

    private var rootView: View? = null
    private var listView: ListView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        //JAVA
//        rootView = View.inflate(context, R.layout.fragment_listview, container)

        //Kotlin
//        rootView = inflater!!.inflate(R.layout.fragment_listview, container, false)

        //Lambda
        rootView = container?.inflate(R.layout.fragment_listview)

        listView = rootView!!.findViewById(R.id.listView) as ListView

        listView!!.onItemClickListener = AdapterView.OnItemClickListener {
            parent, view, position, id
                -> Toast.makeText(context, "" + listView!!.getItemAtPosition(position), Toast.LENGTH_SHORT).show()
        }

        //Lambda
//        imageView.loadUrl("http://....")

        //Lambda
//        rootView!!.snack("SnackBar")
//        rootView!!.snack("SnackBar", Snackbar.LENGTH_SHORT)

//        rootView!!.snack("Snack message") {
//            action("Action") {
//                Toast.makeText(context, "ACTION", Toast.LENGTH_SHORT).show()
//            }
//        }

        //postDelayed
        rootView!!.postDelayed({ Toast.makeText(context, "postDelay", Toast.LENGTH_SHORT).show() }, 3000)

        //Thread
//        Thread().run {
//             Running in a thread
//        }

        //ANKO
        // https://github.com/Kotlin/anko
//        async() {
//            // Do something in a secondary thread
//            uiThread {
//                // Back to the main thread
//            }
//        }

//        var b:String? = ""
//        b = null;

        println("max " + max(4, 6))
        println("maxOneLine " + maxOneLine(4, 6))

        val numbers: MutableList<Int> = mutableListOf(1, 2, 3)
        val readOnlyView: List<Int> = numbers
        println("numbers " + numbers)        // prints "[1, 2, 3]"
        numbers.add(4)
        println("numbers add" + readOnlyView)   // prints "[1, 2, 3, 4]"

        val strings = hashSetOf("a", "b", "c", "c")
        assert(strings.size == 3)

        println(stringToUpperCase("test"))

        println("duplicateDoubles " + duplicateDoubles(arrayOf<Int>(1, 2, 3, 4, 5, 6, 7)))

        val f: (String, Int) -> String = { s, i -> "$s $i" }
        println("f " + f)

        val result2 = doubleTheResult(3, 4, {x,y -> x+y})
        println("result2 " + result2)

        val numberss:Array<Int> = arrayOf(1, 2, 3, 4, 5)

        val squaredNumbers = numberss.map({x -> x * x})
        println("squaredNumbers " + squaredNumbers)

        // Result will be a new array that contains
        // 1, 4, 9, 16, 25

        //Objects
//        data class MyClass(val a:Int, val b:Int, val c:Int)

        //Immutable
//        fun plusOne(a:Int) {
//            a++
//            return a
//        }

        return rootView
    }
}