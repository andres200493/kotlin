package com.example.andres.kotlin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.andres.kotlin.R

/**
 * Created by Andres on 25/07/2016.
 */
class HomeFragment : android.support.v4.app.Fragment() {

    private var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        rootView = inflater!!.inflate(R.layout.fragment_home, container, false)

        return rootView
    }
}
