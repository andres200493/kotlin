package com.example.andres.kotlin.commons

import android.app.Activity
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

/**
 * Created by Andres on 02/08/2016.
 */
fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun ImageView.loadUrl(url: String) {
//    Picasso.with(context).load(url).into(this)
}

// region SnackBar

//fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG, f: (Snackbar.() -> Unit)? = null) {
//    val snack = Snackbar.make(this, message, length)
//    if (f != null) snack.f()
//    snack.show()
//}
//
//fun Snackbar.action(action: String, @ColorInt color: Int? = null, listener: (View) -> Unit) {
//    setAction(action, listener)
//    color?.let { setActionTextColor(color) }
//}

fun View.snack(text: CharSequence, duration: Int = Snackbar.LENGTH_SHORT, init: Snackbar.() -> Unit = {}): Snackbar {
    val snack = Snackbar.make(this, text, duration)
    snack.init()
    snack.show()
    return snack
}

fun Fragment.snackbar(text: CharSequence, duration: Int = Snackbar.LENGTH_LONG, init: Snackbar.() -> Unit = {}): Snackbar {
    return getView()!!.snack(text, duration, init)
}

fun Activity.snackbar(view: View, text: CharSequence, duration: Int = Snackbar.LENGTH_LONG, init: Snackbar.() -> Unit = {}): Snackbar {
    return view.snack(text, duration, init)
}

// endregion SnackBar

fun duplicateDoubles(items: Array<Int>) = items.filter { (it % 2) == 0 }.map { it * 2 }

fun doubleTheResult(x:Int, y:Int, f:(Int, Int)->Int): Int {
    return f(x,y) * 2
}

fun sum(a:Int, b:Int) = a + b

fun stringToUpperCase(a: Any) {
    if (a is String) {
        println(a.toUpperCase()) // Podemos poner toUpperCase directamente porque se sabe que a es de tipo cadena
    }
}

fun max(a: Int, b: Int): Int {
    if (a > b)
        return a
    else
        return b
}

fun maxOneLine(a: Int, b: Int) = if (a > b) a else b
